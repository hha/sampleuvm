import uvm_pkg::*;
`include "uvm_macros.svh"

class alu_test0 extends alu_test;

	`uvm_component_utils(alu_test0)

	function new (string name = "alu_test", uvm_component parent);
		super.new (name, parent);
		// Affect all factory requests for this type
		set_type_override_by_type (alu_seq::get_type(), alu_seq_new::get_type());

		// Affect factory requests for this type only on given path
		set_inst_override_by_type ("env.agent.sequencer.*", alu_seq::get_type(), alu_seq_new::get_type());	

	endfunction : new	

endclass: alu_test0
