import uvm_pkg::*;
`include "uvm_macros.svh"

class alu_driver extends uvm_driver #(alu_req_item);
	alu_req_item s_item;
	virtual alu_if.drv vif;
	
	`uvm_component_utils(alu_driver)

	function new (string name = "alu_driver", uvm_component parent);
		super.new (name, parent);
	endfunction : new	

	virtual function void connect_phase (uvm_phase phase);
		super.connect_phase (phase);
		if (!uvm_config_db #(virtual alu_if.drv)::get(this,"","vif", vif)) begin
			uvm_config_db #(virtual alu_if.drv)::dump();
			`uvm_fatal("NO VIF", {"virtual interface must be set for: ", get_full_name(), ".vif"});
		end
	endfunction: connect_phase

	virtual task run_phase (uvm_phase phase);
		`uvm_info (get_type_name(), "Start run_phase", UVM_MEDIUM)

		forever begin
			seq_item_port.get_next_item (s_item);
			drive_item(s_item);
			seq_item_port.item_done();
		end
	endtask: run_phase

	task drive_item (input alu_req_item item);
		vif.drv_cb.op0 <= item.opr0;
		vif.drv_cb.op1 <= item.opr1;
		vif.drv_cb.add_subn   <= item.op;
		vif.drv_cb.vld  <= item.idle ? 1'b0 : 1'b1;
		@(vif.drv_cb);
	endtask: drive_item

endclass: alu_driver
