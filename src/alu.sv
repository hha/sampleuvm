module alu (alu_if.des aif);
	logic [7:0] nxt_ans;
	logic       nxt_c;
	
	assign {nxt_c, nxt_ans} = aif.add_subn == 1'b1 ? aif.op0 + aif.op1 : aif.op0 - aif.op1;

	always_ff @(posedge aif.clk or negedge aif.rstn) begin
		if (~aif.rstn) begin
			aif.ans     <= 8'd0;
			aif.c       <= 1'b0;
			aif.ans_vld <= 1'b0;
		end else begin
			aif.ans     <= nxt_ans;
			aif.c       <= nxt_c;
			aif.ans_vld <= aif.vld;
		end 
	end

endmodule: alu 
