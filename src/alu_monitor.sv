import uvm_pkg::*;
`include "uvm_macros.svh"

class alu_monitor extends uvm_monitor;
	virtual alu_if.mon vif;
	bit checks_enable = 1;
	bit coverage_enable = 1;

	uvm_analysis_port #(alu_req_item) req_collected_port;
	uvm_analysis_port #(alu_rsp_item) ans_collected_port;
	
	event cov_transaction;
	
	protected alu_req_item req_collected;
	protected alu_rsp_item ans_collected;
	
	`uvm_component_utils_begin(alu_monitor)
		`uvm_field_int(coverage_enable, UVM_ALL_ON)
	`uvm_component_utils_end

	covergroup cov_trans @cov_transaction;
		option.per_instance = 1;
		// TODO
	endgroup: cov_trans

	function new (string name = "alu_monitor", uvm_component parent);
		super.new (name, parent);
		cov_trans = new ();
		cov_trans.set_inst_name ({get_full_name(), ".cov_trans"});
		req_collected = new();
		ans_collected = new();
		req_collected_port = new ("req_collected_port", this);
		ans_collected_port = new ("ans_collected_port", this);
	endfunction : new	

	virtual function void connect_phase (uvm_phase phase);
		super.connect_phase (phase);

		if (!uvm_config_db #(virtual alu_if.mon)::get(this,"","vif", vif)) begin
			uvm_config_db #(virtual alu_if.mon)::dump();
			`uvm_fatal("NO VIF", {"virtual interface must be set for: ", get_full_name(), ".vif"});
		end
	endfunction: connect_phase

	virtual task run_phase (uvm_phase phase);
		collect_transactions ();
	endtask: run_phase

	virtual protected task collect_transactions ();
		forever begin
			@(vif.mon_cb);
			if (vif.mon_cb.vld == 1'b1) begin
				req_collected.opr0 = vif.mon_cb.op0;
				req_collected.opr1 = vif.mon_cb.op1;
				req_collected.op   = vif.mon_cb.add_subn;

				req_collected_port.write(req_collected);
			end

			if (vif.mon_cb.ans_vld == 1'b1) begin
				ans_collected.ans   = vif.mon_cb.ans;
				ans_collected.carry = vif.mon_cb.c;

				ans_collected_port.write(ans_collected);
			end

			if (coverage_enable)
				perform_coverage();

		end	
	endtask: collect_transactions

	virtual protected function void perform_coverage();
		-> cov_transaction;
	endfunction: perform_coverage

endclass: alu_monitor
