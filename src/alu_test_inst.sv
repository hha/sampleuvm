import uvm_pkg::*;
`include "uvm_macros.svh"

class alu_test_inst extends alu_test;
	alu_seq auto_seq;

	`uvm_component_utils(alu_test_inst)

	function new (string name = "alu_test_inst", uvm_component parent);
		super.new (name, parent);
	endfunction : new	

	virtual function void set_default_sequences ();
		uvm_config_db#(uvm_object_wrapper)::set(this, "env.agent.sequencer.reset_phase", "default_sequence", alu_rst_seq::type_id::get());
		uvm_config_db#(uvm_object_wrapper)::set(this, "env.agent.sequencer.shutdown_phase", "default_sequence", alu_rst_seq::type_id::get());

		// Need to create, randomize sequence instance manually before entering specified phase
		auto_seq = alu_seq::type_id::create("auto_seq_inst",, get_full_name());
		assert (auto_seq.randomize());
		uvm_config_db#(uvm_sequence_base)::set(this, "env.agent.sequencer.main_phase", "default_sequence", auto_seq);
		uvm_config_db#(uvm_sequence_base)::dump ();
	endfunction: set_default_sequences

endclass: alu_test_inst
