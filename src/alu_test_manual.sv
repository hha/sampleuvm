import uvm_pkg::*;
`include "uvm_macros.svh"

class alu_test_manual extends alu_test;
	alu_seq manual_seq;

	`uvm_component_utils(alu_test_manual)

	function new (string name = "alu_test_manual", uvm_component parent);
		super.new (name, parent);
	endfunction : new	

	virtual task main_phase (uvm_phase phase);
		super.run_phase (phase);
		phase.raise_objection (this);
		
		//Manual start sequence
		manual_seq = alu_seq::type_id::create("manual_seq",, get_full_name());
		assert (manual_seq.randomize());
		manual_seq.start (env.agent.sequencer);

		phase.drop_objection (this);
	endtask: main_phase

	virtual function void set_default_sequences ();
		// Automatically phase-based sequence starting
		// Specified sequencer will create, randomize and start executing sequence instance
		uvm_config_db#(uvm_object_wrapper)::set(this, "env.agent.sequencer.reset_phase", "default_sequence", alu_rst_seq::type_id::get());
		uvm_config_db#(uvm_object_wrapper)::set(this, "env.agent.sequencer.shutdown_phase", "default_sequence", alu_rst_seq::type_id::get());
	endfunction: set_default_sequences

endclass: alu_test_manual
