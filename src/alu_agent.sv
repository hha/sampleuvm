import uvm_pkg::*;
`include "uvm_macros.svh"

class alu_agent extends uvm_agent;
	`uvm_component_utils(alu_agent)

	function new (string name = "alu_agent", uvm_component parent);
		super.new (name, parent);
	endfunction : new	
	
	alu_driver driver;
	alu_monitor monitor;
	alu_sequencer sequencer;

	alu_scoreboard scoreboard;

	virtual function void build_phase (uvm_phase phase);
		super.build_phase (phase);
		monitor = alu_monitor::type_id::create("monitor", this);
		scoreboard = alu_scoreboard::type_id::create("scoreboard", this);
		if (is_active == UVM_ACTIVE) begin
			sequencer = alu_sequencer::type_id::create("sequencer", this);
			driver    = alu_driver::type_id::create("driver", this);
		end
	endfunction: build_phase

	virtual function void connect_phase (uvm_phase phase);
		if (is_active == UVM_ACTIVE) begin
			driver.seq_item_port.connect(sequencer.seq_item_export);
		end

		monitor.req_collected_port.connect (scoreboard.req_collected_export);
		monitor.ans_collected_port.connect (scoreboard.ans_collected_export);
	endfunction: connect_phase

endclass: alu_agent
