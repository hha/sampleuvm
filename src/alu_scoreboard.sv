import uvm_pkg::*;
`include "uvm_macros.svh"

typedef struct {
	bit [7:0] op1;
	bit [7:0] op2;
	bit       op;
	bit [7:0] ans;
	bit 			carry;
} transferStr;

class alu_scoreboard extends uvm_scoreboard;
	bit checks_enable = 1;
	bit flyby_checks_enable = 1;

	transferStr scoreboard[$];

	uvm_analysis_export #(alu_req_item) req_collected_export;
	uvm_tlm_analysis_fifo #(alu_req_item) req_collected_ff;

	uvm_analysis_export #(alu_rsp_item) ans_collected_export;
	uvm_tlm_analysis_fifo #(alu_rsp_item) ans_collected_ff;
	
	`uvm_component_utils_begin(alu_scoreboard)
		`uvm_field_int(checks_enable, UVM_ALL_ON)
		`uvm_field_int(flyby_checks_enable, UVM_ALL_ON)
	`uvm_component_utils_end

	function new (string name = "alu_scoreboard", uvm_component parent);
		super.new (name, parent);
		req_collected_ff = new ("req_collected_ff", this);
		ans_collected_ff = new ("ans_collected_ff", this);
		req_collected_export = new ("req_collected_export", this);
		ans_collected_export = new ("ans_collected_export", this);
	endfunction : new	

	virtual function void connect_phase (uvm_phase phase);
		super.connect_phase (phase);
		assert (uvm_config_db #(int)::get(this,"","checks_enable", checks_enable)) else checks_enable = 1;
		assert (uvm_config_db #(int)::get(this,"","flyby_checks_enable", flyby_checks_enable)) else flyby_checks_enable = 1;
		req_collected_export.connect (req_collected_ff.analysis_export); 
		ans_collected_export.connect (ans_collected_ff.analysis_export); 
	endfunction: connect_phase

	virtual task run_phase (uvm_phase phase);
		alu_req_item req_item;
		alu_rsp_item ans_item;

		super.run_phase (phase);

		forever begin
			transferStr item;
			string s;

			req_collected_ff.get_peek_export.get (req_item);
			item.op1 = req_item.opr0;
			item.op2 = req_item.opr1;
			item.op  = req_item.op;

			ans_collected_ff.get_peek_export.get (ans_item);
			item.ans = ans_item.ans;
			item.carry = ans_item.carry;

			`uvm_info ("Scoreboard", $sformatf ("%0x %s %0x : %0x %0x", item.op1, item.op ? "+" : "-", item.op2, item.carry, item.ans), UVM_MEDIUM); 

			scoreboard.push_back (item);

			if (flyby_checks_enable == 1) my_compare ('{req_item.opr0[7:0], req_item.opr1[7:0], req_item.op[0], ans_item.ans[7:0], ans_item.carry[0]});

		end

	endtask: run_phase

	virtual function void my_compare (transferStr item);
		bit [7:0] exp_ans;
		bit       exp_carry;

		{exp_carry, exp_ans} = item.op == 1 ? item.op1 + item.op2 : item.op1 - item.op2;

		if ((exp_carry != item.carry) || (exp_ans != item.ans)) begin
			`uvm_error ("Scoreboard", $sformatf ("Mismatch result: %0x %s %0x = {%0x, %0x}, received {%0x, %0x}", item.op1, item.op ? "+" : "-", item.op2, exp_carry, exp_ans, item.carry, item.ans))
		end

	endfunction: my_compare

	virtual function void extract_phase (uvm_phase phase);
		alu_req_item req_item;
		alu_rsp_item ans_item;

		super.extract_phase (phase);
		if (req_collected_ff.try_get (req_item))
			`uvm_error ("req_collected_ff", "There are outstanding requests");

		if (ans_collected_ff.try_get (ans_item))
			`uvm_error ("ans_collected_ff", "There are outstanding requests");
	endfunction: extract_phase

	virtual function void check_phase (uvm_phase phase);
		super.check_phase (phase);
		if (checks_enable) begin
			check_result ();
		end
	endfunction: check_phase

	virtual function void check_result ();
		while (scoreboard.size () > 0)
			my_compare (scoreboard.pop_front());
	endfunction: check_result

	virtual function void report_phase (uvm_phase phase);
		uvm_report_server rpt;
	
		super.report_phase (phase);
		rpt = uvm_report_server::get_server();

		if (rpt.get_severity_count(UVM_FATAL) + rpt.get_severity_count(UVM_ERROR) > 0)
			`uvm_info ("Final", "STATUS: FAILED", UVM_LOW)
		else
			`uvm_info ("Final", "STATUS: PASSED", UVM_LOW)

	endfunction: report_phase

endclass: alu_scoreboard
