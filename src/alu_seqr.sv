import uvm_pkg::*;
`include "uvm_macros.svh"

class alu_sequencer extends uvm_sequencer #(alu_req_item);
	`uvm_component_utils(alu_sequencer)

	function new (string name = "alu_sequencer", uvm_component parent);
		super.new (name, parent);
	endfunction : new	

endclass: alu_sequencer
