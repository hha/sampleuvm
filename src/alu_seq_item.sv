import uvm_pkg::*;
`include "uvm_macros.svh"

typedef enum {ZERO, MAX, RANDOM} operands_e;

class alu_req_item extends uvm_sequence_item;
	rand bit idle;
	rand int unsigned opr0;
	rand int unsigned opr1;
	rand bit          op;
	rand operands_e   opr_cat;

	`uvm_object_utils_begin(alu_req_item)
		`uvm_field_int(opr0, UVM_ALL_ON)
		`uvm_field_int(opr1, UVM_ALL_ON)
		`uvm_field_int(op,   UVM_ALL_ON)
		`uvm_field_int(idle, UVM_ALL_ON)
		`uvm_field_enum(operands_e, opr_cat, UVM_ALL_ON)
	`uvm_object_utils_end

	constraint opr_cat_c {opr_cat dist {ZERO:=1, MAX:=1, RANDOM:=8};};

	constraint opr0_c { (opr_cat == ZERO) -> opr0 == 0;
									(opr_cat == MAX) -> opr0 == 255;
									(opr_cat == RANDOM) -> opr0 inside {[0:255]};}

	constraint opr1_c { (opr_cat == ZERO) -> opr1 == 0;
									(opr_cat == MAX) -> opr1 == 255;
									 (opr_cat == RANDOM) -> opr1 inside {[0:255]};}

	constraint o_c { op dist {1'b0 := 1, 1'b1 := 1};}

	constraint idle_c { idle dist {1'b0 := 7, 1'b1 := 3};}

	function new (string name = "alu_req_item");
		super.new(name);
	endfunction: new
endclass: alu_req_item

class alu_rsp_item extends uvm_sequence_item;
	int unsigned      ans;
	bit           carry;

	`uvm_object_utils_begin(alu_rsp_item)
		`uvm_field_int(ans,  UVM_ALL_ON) 
		`uvm_field_int(carry,UVM_ALL_ON)
	`uvm_object_utils_end

	function new (string name = "alu_rsp_item");
		super.new(name);
	endfunction: new
endclass: alu_rsp_item
