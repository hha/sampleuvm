import uvm_pkg::*;
`include "uvm_macros.svh"

class alu_seq extends uvm_sequence #(alu_req_item);
	rand int count;
	constraint iteration_c {count > 0; count < 50;}

	function new (string name = "alu_seq");
		super.new (name);
		set_automatic_phase_objection (1);
	endfunction : new	

	`uvm_object_utils(alu_seq)

	virtual task body();
		req = alu_req_item::type_id::create ("req");
		`uvm_info (get_type_name(), $sformatf ("Sequence %s has %d items", this.get_name(), count), UVM_MEDIUM)

		repeat(count) begin
			start_item (req);
			assert (req.randomize ());
//			req.print (); // For debug only
			finish_item (req);
		end
		
		`uvm_info (get_type_name (), $sformatf ("Sequence %s is done", this.get_name()), UVM_MEDIUM)
	endtask: body

endclass: alu_seq

class alu_rst_seq extends uvm_sequence #(alu_req_item);
	int count = 2;

	function new (string name = "alu_rst_seq");
		super.new (name);
		set_automatic_phase_objection (1);
	endfunction : new	

	`uvm_object_utils(alu_rst_seq)

	virtual task body();
		req = alu_req_item::type_id::create ("req");

		repeat(count) begin
			start_item (req);
			assert (req.randomize () with {idle == 1;});
			finish_item (req);
		end
		
		`uvm_info (get_type_name (), $sformatf ("Sequence %s is done", this.get_name()), UVM_MEDIUM)
	endtask: body

endclass: alu_rst_seq
