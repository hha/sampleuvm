import uvm_pkg::*;
`include "uvm_macros.svh"

class alu_seq_new extends alu_seq;
	function new (string name = "alu_seq_new");
		super.new (name);
	endfunction : new	

	`uvm_object_utils(alu_seq_new)

endclass: alu_seq_new
