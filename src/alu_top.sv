module top;
	import uvm_pkg::*;

	logic clk, rstn;

	alu_if  alu_if (clk, rstn);
	alu     dut (alu_if.des);

	initial begin
		automatic uvm_coreservice_t test = uvm_coreservice_t::get();
		uvm_config_db #(virtual alu_if.drv)::set(test.get_root(), "uvm_test_top.env.agent.driver", "vif", alu_if.drv);
		uvm_config_db #(virtual alu_if.mon)::set(test.get_root(), "uvm_test_top.env.agent.monitor", "vif", alu_if.mon);
		run_test ("alu_test");
	end

	initial begin
		clk  <= 1'b0;	
		rstn <= 1'b0;
		#25 rstn <= 1'b1;
	end
	
	initial begin
		forever begin
			#(10) clk = ~clk;
		end
	end

	initial begin
		$dumpfile("dump.vcd");
		$dumpvars(0, top);
	end

endmodule
