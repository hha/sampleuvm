interface alu_if (input logic clk, rstn);
	logic [7:0] op0;
	logic [7:0] op1;
	logic       add_subn;
	logic       vld;

	logic [7:0] ans;
	logic       ans_vld;
	logic c;

	modport des (input clk, rstn, op0, op1, add_subn, vld, output ans, c, ans_vld);

	clocking drv_cb @(posedge clk);
		output op0, op1, add_subn, vld;
		input ans, ans_vld, c;
	endclocking: drv_cb 

	clocking mon_cb @(posedge clk);
		input op0, op1, add_subn, vld, ans, ans_vld, c;
	endclocking: mon_cb

	modport drv (input rstn, clocking drv_cb);
	modport mon (input rstn, clocking mon_cb);

endinterface


