import uvm_pkg::*;
`include "uvm_macros.svh"

class alu_test extends uvm_test;
	alu_env env;

	`uvm_component_utils(alu_test)

	function new (string name = "alu_test", uvm_component parent);
		super.new (name, parent);
	endfunction : new	

	virtual function void build_phase (uvm_phase phase);
		super.build_phase (phase);
		env = alu_env::type_id::create ("env", this);
		set_env ();
		set_default_sequences ();
	endfunction: build_phase
	
	virtual function void connect_phase (uvm_phase phase);
		uvm_top.print_topology;
	endfunction: connect_phase

	virtual function void set_env ();
		uvm_config_db#(int)::set(this, "*", "checks_enable", 1);
		uvm_config_db#(int)::set(this, "*", "flyby_checks_enable", 0);
	endfunction: set_env

	virtual function void set_default_sequences ();
		// Automatically phase-based sequence starting
		// Specified sequencer will create, randomize and start executing sequence instance
		uvm_config_db#(uvm_object_wrapper)::set(this, "env.agent.sequencer.reset_phase", "default_sequence", alu_rst_seq::type_id::get());
		uvm_config_db#(uvm_object_wrapper)::set(this, "env.agent.sequencer.shutdown_phase", "default_sequence", alu_rst_seq::type_id::get());

		uvm_config_db#(uvm_object_wrapper)::set(this, "env.agent.sequencer.main_phase", "default_sequence", alu_seq::type_id::get());

		uvm_config_db#(uvm_object_wrapper)::dump();
	endfunction: set_default_sequences

endclass: alu_test
