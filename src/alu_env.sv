import uvm_pkg::*;
`include "uvm_macros.svh"

class alu_env extends uvm_env;
	alu_agent agent;

	`uvm_component_utils(alu_env)

	function new (string name = "alu_env", uvm_component parent);
		super.new (name, parent);
	endfunction : new	
	
	virtual function void build_phase (uvm_phase phase);
		super.build_phase (phase);
		agent = alu_agent::type_id::create("agent", this);
	endfunction: build_phase

endclass: alu_env
